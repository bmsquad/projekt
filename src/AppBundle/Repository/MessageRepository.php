<?php
namespace AppBundle\Repository;

use AppBundle\Entity\LastMessageView;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class MessageRepository extends EntityRepository{

    public function getWhere($a, $b){
        $expr = new Expr();
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->where($expr->andX(
                $expr->eq('m.receiver', $a instanceof User ? $a->getId() : $a),
                $expr->eq('m.sender', $b instanceof User ? $b->getId() : $b))
            )
            ->orWhere($expr->andX(
                $expr->eq('m.receiver', $b instanceof User ? $b->getId() : $b),
                $expr->eq('m.sender', $a instanceof User ? $a->getId() : $a))
            );
        return $qb->getDQLPart('where');
    }

    public function getMessages($a, $b, $page = 1){
        return $this->createQueryBuilder('m')
            ->where($this->getWhere($a, $b))
            ->setMaxResults(10)
            ->setFirstResult(($page - 1) * 10)
            ->orderBy('m.date', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getNewMessages($a, $b, $id){
        return $this->createQueryBuilder('m')
            ->where($this->getWhere($a, $b))
            ->andWhere('m.id > :id')
            ->setParameter('id', $id)
            ->orderBy('m.date', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getAllLastMessages($user){
        return $this->_em->getRepository(LastMessageView::class)->getAllLastMessages($user);
//        $rsm = new ResultSetMappingBuilder($this->_em, ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
//        $rsm->addRootEntityFromClassMetadata(Message::class, 'm');
//        $rsm->addJoinedEntityFromClassMetadata(User::class, 's', 'm', 'sender');
//        $rsm->addJoinedEntityFromClassMetadata(User::class, 'r', 'm', 'receiver');
//        $select = $rsm->generateSelectClause(array(
//            'message' => 'm',
//            'receiver' => 'r',
//            'sender' => 's'
//        ));
//        $sql = 'SELECT ' . $select . ' from message m inner join users s on m.sender = s.id inner join users r on m.receiver = r.id where m.id in (SELECT max(id) as i from message m group by if(sender < receiver, concat(sender, " ", receiver), concat(receiver, " ", sender))) and (m.receiver = :user or m.sender = :user) order by m.date desc';
//        $query = $this->_em->createNativeQuery($sql, $rsm);
//        $query->setParameter('user', $user->getId());
//        return $query->getResult();
    }
}
