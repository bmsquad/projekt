<?php
    namespace AppBundle\Repository;

    use AppBundle\Entity\Advertisement;
    use AppBundle\Entity\Application;
    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\Query\Expr;

    class AdvertisementRepository extends EntityRepository{

        public function getNewest(){
            $expr = new Expr();
            return $this->createQueryBuilder('a')
                ->addSelect('u, c, r')
                ->leftJoin('a.user', 'u')
                ->leftJoin('u.company', 'c')
                ->leftJoin('a.requirements', 'r')
                ->where($expr->in(
                    'a.id',
                        $this->_em->createQueryBuilder()
                            ->select('ad.id')
                            ->from(Advertisement::class, 'ad')
                            ->orderBy('ad.date', 'desc')
                            ->setMaxResults(5)
                            ->getDQL()
                    ))
                ->getQuery()
                ->getResult();
        }

        public function getWithApplications($id){
            $expr = new Expr();
            try{
                $ad = $this->createQueryBuilder('a')
                    ->addSelect('ap, u')
                    ->leftJoin('a.applications', 'ap')
                    ->leftJoin('ap.user', 'u')
                    ->where('a.id = :id')
                    ->andWhere($expr->in(
                        'ap.id',
                        $this->_em->createQueryBuilder()
                            ->select('max(app.id)')
                            ->from(Application::class, 'app')
                            ->groupBy('app.user, app.ad')
                            ->getDQL()
                    ))
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getSingleResult();
            }catch(\Exception $e){
                $ad = null;
            }

            if($ad)
                return $ad;

            try{
                return $this->createQueryBuilder('a')
                    ->where('a.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getSingleResult();
            }catch(\Exception $e){
                return null;
            }

        }

        public function getWithRequirements($id){
            try{
                return $this->createQueryBuilder('a')
                    ->addSelect('r, u, c')
                    ->leftJoin('a.requirements', 'r')
                    ->leftJoin('a.user', 'u')
                    ->leftJoin('u.company', 'c')
                    ->where('a.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getSingleResult();
            }catch(\Exception $e){
                return null;
            }
        }

        public function search($value = ''){
            return $this->createQueryBuilder('a')
                ->addSelect('u')
                ->leftJoin('a.user','u')
                ->where('a.tags like :value')
                ->setParameter('value', "%$value%")
                ->setMaxResults(5)
                ->getQuery()
                ->getResult();
        }
    }