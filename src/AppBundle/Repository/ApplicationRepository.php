<?php
    namespace AppBundle\Repository;

    use AppBundle\Entity\Advertisement;
    use AppBundle\Entity\User;
    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\Query\Expr;

    class ApplicationRepository extends EntityRepository{

        public function hasApplicated($user, $ad){
            try{
                return $this->createQueryBuilder('a')
                    ->where('a.user = :user')
                    ->andWhere('a.ad = :ad')
                    ->orderBy('a.date', 'desc')
                    ->setParameters(array('user' => $user instanceof User ? $user->getId() : $user, 'ad' => $ad instanceof Advertisement ? $ad->getId() : $ad))
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getSingleResult();
            }catch(\Exception $e){
                return null;
            }
        }

        public function getNewest($user){
            $expr = new Expr();
            return $this->createQueryBuilder('a')
                ->addSelect('u, adv')
                ->leftJoin('a.user', 'u')
                ->leftJoin('a.ad', 'adv')
                ->where($expr->in(
                    'a.ad', $this->_em->createQueryBuilder()
                        ->select('ad.id')
                        ->from(Advertisement::class, 'ad')
                        ->where('ad.user = :user')
                        ->orderBy('a.date', 'desc')
                        ->getDQL()
                    )
                )
                ->setParameter('user', $user->getId())
                ->setMaxResults(5)
                ->getQuery()
                ->getResult();
        }

        public function getPage($user, $limit, $page){
            return $this->createQueryBuilder('a')
                ->addSelect('u, ad')
                ->leftJoin('a.ad', 'ad')
                ->leftJoin('ad.user', 'u')
                ->where('a.user = :user')
                ->orderBy('a.date', 'desc')
                ->setParameter('user', $user)
                ->setMaxResults($limit)
                ->setFirstResult(($page - 1) * $limit)
                ->getQuery()
                ->getResult();
        }
    }