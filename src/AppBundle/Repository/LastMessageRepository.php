<?php
    namespace AppBundle\Repository;

    use Doctrine\ORM\EntityRepository;
    use Doctrine\ORM\Mapping as ORM;

    class LastMessageRepository extends EntityRepository{

        public function getAllLastMessages($user){
            return $this->createQueryBuilder('lm')
                ->addSelect('s, r')
                ->leftJoin('lm.sender', 's')
                ->leftJoin('lm.receiver', 'r')
                ->where('lm.sender = :user')
                ->orWhere('lm.receiver = :user')
                ->setParameter('user', $user)
                ->getQuery()
                ->getResult();
        }
    }