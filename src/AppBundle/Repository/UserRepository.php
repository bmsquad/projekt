<?php
    namespace AppBundle\Repository;

    use Doctrine\ORM\EntityRepository;

    class UserRepository extends EntityRepository{

        public function getWithJoins($user, $employer = false){
            try{
                if($employer){
                    return $this->createQueryBuilder('u')
                        ->addSelect('c')
                        ->leftJoin('u.company', 'c')
                        ->where('u.id = :user')
                        ->setParameter('user', $user)
                        ->getQuery()
                        ->getOneOrNullResult();
                }else{
                    return $this->createQueryBuilder('u')
                        ->addSelect('exp, ed, lang, s')
                        ->leftJoin('u.experience', 'exp')
                        ->leftJoin('u.education', 'ed')
                        ->leftJoin('u.language', 'lang')
                        ->leftJoin('u.skill', 's')
                        ->where('u.id = :user')
                        ->setParameter('user', $user)
                        ->getQuery()
                        ->getOneOrNullResult();
                }
            }catch(\Exception $e){
                return null;
            }
        }

        public function search($user, $me){
            return $this->createQueryBuilder('u')
                ->where("CONCAT(u.name,' ',u.surname) LIKE :user AND u.id NOT LIKE :me")
                ->setParameter('user', '%' . $user . '%')
                ->setParameter('me', $me)
                ->setMaxResults(5)
                ->getQuery()
                ->getResult();
        }
    }