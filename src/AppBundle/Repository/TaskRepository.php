<?php
    namespace AppBundle\Repository;

    use Doctrine\ORM\EntityRepository;

    class TaskRepository extends EntityRepository{

        public function getWithJoins($id){
            try{
                return $this->createQueryBuilder('t')
                    ->addSelect('ap, u')
                    ->leftJoin('t.application', 'ap')
                    ->leftJoin('ap.user', 'u')
                    ->where('t.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getSingleResult();
            }catch(\Exception $e){
                return null;
            }
        }
    }