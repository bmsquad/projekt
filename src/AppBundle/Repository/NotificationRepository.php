<?php
    namespace AppBundle\Repository;

    use AppBundle\Entity\Notification;
    use Doctrine\ORM\EntityRepository;

    class NotificationRepository extends EntityRepository{

        public function getLastNotifications($user, $limit, $amount){
            return $this->createQueryBuilder('m')
                ->where('m.user = :user')
                ->orderBy('m.date', 'desc')
                ->setMaxResults($limit)
                ->setFirstResult($amount)
                ->setParameter('user', $user)
                ->getQuery()
                ->getResult();
        }
//        public function getLastNotifications($user, $limit, $page){
//            return $this->createQueryBuilder('m')
//                ->where('m.user = :user')
//                ->orderBy('m.date', 'desc')
//                ->setMaxResults($limit)
//                ->setFirstResult(($page - 1) * $limit)
//                ->setParameter('user', $user)
//                ->getQuery()
//                ->getResult();
//        }

        public function setSeen($notification, $value){
            return $this->_em->createQueryBuilder()
                ->update(Notification::class, 'n')
                ->set('n.seen', $value)
                ->where('n.id = :id')
                ->setParameter('id', $notification)
                ->getQuery()
                ->execute();
        }
    }