<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\NotificationRepository")
     * @ORM\Table(name="notifications")
     */
    class Notification{

        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumn(name="user", referencedColumnName="id")
         */
        private $user;

        /**
         * @ORM\Column(type="string", length=255)
         */
        private $value;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         */
        private $link;

        /**
         * @ORM\Column(type="boolean")
         */
        private $seen = false;

        /**
         * @ORM\Column(type="datetime")
         */
        private $date;

        public function __construct($user, $value, $link = null){
            $this->user = $user;
            $this->value = $value;
            $this->link = $link;
            $this->date = new \DateTime();
        }

        public function getId(){
            return $this->id;
        }

        public function getUser(){
            return $this->user;
        }

        public function getValue(){
            return $this->value;
        }

        public function getLink(){
            return $this->link;
        }

        public function getSeen(){
            return $this->seen;
        }

        public function getDate(){
            return $this->date;
        }
    }