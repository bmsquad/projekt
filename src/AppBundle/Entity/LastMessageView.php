<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\LastMessageRepository", readOnly=true)
     * @ORM\Table(name="last_msg")
     */
    class LastMessageView{
        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\Column(type="string")
         */
        private $value;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumn(name="sender", referencedColumnName="id")
         */
        private $sender;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumn(name="receiver", referencedColumnName="id")
         */
        private $receiver;

        /**
         * @ORM\Column(type="boolean")
         */
        private $seen = false;

        /**
         * @ORM\Column(type="datetime")
         */
        private $date;

        public function getId(){
            return $this->id;
        }

        public function getValue(){
            return $this->value;
        }

        public function getSender(){
            return $this->sender;
        }

        public function getReceiver(){
            return $this->receiver;
        }

        public function getSeen(){
            return $this->seen;
        }

        public function getDate(){
            return $this->date;
        }
    }