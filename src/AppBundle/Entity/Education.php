<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity
     * @ORM\Table(name="education")
     */
    class Education{

        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\Column(type="string")
         */
        private $name;

        /**
         * @ORM\Column(type="string")
         */
        private $direction;

        /**
         * @ORM\Column(type="string")
         */
        private $level;

        /**
         * @ORM\Column(type="date")
         */
        private $datefrom;

        /**
         * @ORM\Column(type="date")
         */
        private $dateto;

        /**
         * @ORM\ManyToOne(targetEntity="User", inversedBy="education")
         * @ORM\JoinColumn(name="user", referencedColumnName="id")
         */
        private $user;

        const name = 'Edukacja';

        public function getClass(){
            return get_class($this);
        }

        public function getId(){
            return $this->id;
        }

        public function getName(){
            return $this->name;
        }

        public function setName($val){
            $this->name = $val;
            return $this;
        }

        public function getDirection(){
            return $this->direction;
        }

        public function setDirection($val){
            $this->direction = $val;
            return $this;
        }

        public function getLevel(){
            return $this->level;
        }

        public function setLevel($val){
            $this->level = $val;
            return $this;
        }

        public function getDateFrom(){
            return $this->datefrom;
        }

        public function setDateFrom($val){
            $this->datefrom = $val;
            return $this;
        }

        public function getDateTo(){
            return $this->dateto;
        }

        public function setDateTo($val){
            $this->dateto = $val;
            return $this;
        }

        public function getUser(){
            return $this->user;
        }

        public function setUser($val){
            $this->user = $val;
            return $this;
        }
    }