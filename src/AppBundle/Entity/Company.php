<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity
     * @ORM\Table(name="companies", indexes={@ORM\Index(name="c_user", columns={"user"})})
     */
    class Company{

        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\Column(type="string", length=10)
         */
        private $nip;

        /**
         * @ORM\Column(type="string", length=255)
         */
        private $name;

        /**
         * @ORM\Column(type="string", length=75)
         */
        private $person;

        /**
         * @ORM\Column(type="string", length=100)
         */
        private $address;

        /**
         * @ORM\Column(type="string", length=60)
         */
        private $post;

        /**
         * @ORM\Column(type="string", length=15, nullable=true)
         */
        private $phone;

        /**
         * @ORM\Column(type="string", length=50, nullable=true)
         */
        private $mail;

        /**
         * @ORM\Column(type="string", length=100, nullable=true)
         */
        private $website;

        /**
         * @ORM\ManyToOne(targetEntity="User", inversedBy="company")
         * @ORM\JoinColumn(name="user", referencedColumnName="id")
         */
        private $user;

        public function __construct($company, $user){
            $this->createFromApiData($company, $user);
        }

        public function createFromApiData($company, $user){
            $this->nip = $company->nip;
            $this->name = $company->name;
            $this->person = $company->firstname . ' ' . $company->lastname;
            $this->address = $company->street . ' '
                . $company->streetNumber
                . ($company->houseNumber ? '/' . $company->houseNumber : '') . ' '
                . $company->city;
            $this->post = $company->postCode . ' ' . $company->postCity;
            $this->phone = $company->phone ?: null;
            $this->mail = $company->email ?: null;
            $this->website = $company->www ?: null;
            $this->user = $user;
        }

        public function getId(){
            return $this->id;
        }

        public function getNip(){
            return $this->nip;
        }

        public function setNip($val){
            $this->nip = $val;
            return $this;
        }

        public function getName(){
            return $this->name;
        }

        public function setName($val){
            $this->name = $val;
            return $this;
        }

        public function getPerson(){
            return $this->person;
        }

        public function setPerson($val){
            $this->person = $val;
            return $this;
        }

        public function getAddress(){
            return $this->address;
        }

        public function setAddress($val){
            $this->address = $val;
            return $this;
        }

        public function getPost(){
            return $this->post;
        }

        public function setPost($val){
            $this->post = $val;
            return $this;
        }

        public function getPhone(){
            return $this->phone;
        }

        public function setPhone($val){
            $this->phone = $val;
            return $this;
        }

        public function getMail(){
            return $this->mail;
        }

        public function setMail($val){
            $this->mail = $val;
            return $this;
        }

        public function getWebsite(){
            return $this->website;
        }

        public function setWebsite($val){
            $this->website = $val;
            return $this;
        }

        public function getUser(){
            return $this->user;
        }

        public function setUser($val){
            $this->user = $val;
            return $this;
        }
    }