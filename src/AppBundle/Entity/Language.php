<?php

    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity
     * @ORM\Table(name="language")
     */
    class Language {

        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\Column(type="string")
         */
        private $language;

        /**
         * @ORM\Column(type="string")
         */
        private $level;

        /**
         * @ORM\ManyToOne(targetEntity="User", inversedBy="language")
         * @ORM\JoinColumn(name="user", referencedColumnName="id")
         */
        private $user;

        const name = 'Języki';

        public function getClass(){
            return get_class($this);
        }

        public function getId(){
            return $this->id;
        }

        public function getLanguage(){
            return $this->language;
        }

        public function setLanguage($val){
            $this->language = $val;
            return $this;
        }

        public function getLevel(){
            return $this->level;
        }

        public function setLevel($val){
            $this->level = $val;
            return $this;
        }

        public function getUser(){
            return $this->user;
        }

        public function setUser($val){
            $this->user = $val;
            return $this;
        }

    }