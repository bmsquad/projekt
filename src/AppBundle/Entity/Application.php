<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicationRepository")
     * @ORM\Table(name="applications")
     */
    class Application{

        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumn(name="user", referencedColumnName="id")
         */
        private $user;

        /**
         * @ORM\ManyToOne(targetEntity="Advertisement", inversedBy="applications")
         * @ORM\JoinColumn(name="ad", referencedColumnName="id")
         */
        private $ad;

        /**
         * @ORM\Column(type="datetime")
         */
        private $date;

        /**
         * @ORM\Column(type="smallint")
         */
        private $status;

        /**
         * @ORM\OneToMany(targetEntity="Task", mappedBy="application")
         */
        private $tasks;

        const STATUS_WAITING = 0;
        const STATUS_ACCEPTED = 1;
        const STATUS_REJECTED = 2;
        const STATUS_TASK = 3;
        const STATUS_TASK_ANSWERED = 4;

        public static function getStatusString($status){
            switch($status){
                case self::STATUS_WAITING:{
                    return array('status' => 'Oczekuje na decyzję pracodawcy', 'class' => 'text-main');
                }
                case self::STATUS_ACCEPTED:{
                    return array('status' => 'Aplikacja rozpatrzona pozytywnie', 'class' => 'text-success');
                }
                case self::STATUS_REJECTED:{
                    return array('status' => 'Aplikacja odrzucona', 'class' => 'text-danger');
                }
                case self::STATUS_TASK:{
                    return array('status' => 'Przyznano zadanie rekrutacyjne', 'class' => 'text-main');
                }
                case self::STATUS_TASK_ANSWERED:{
                    return array('status' => 'Przesłano zadanie rekrutacyjne', 'class' => 'text-main');
                }
                default:{
                    return '';
                }
            }
        }

        public function __construct($user, $ad){
            $this->user = $user;
            $this->ad = $ad;
            $this->date = new \DateTime();
            $this->status = self::STATUS_WAITING;
        }

        public function getId(){
            return $this->id;
        }

        public function getUser(){
            return $this->user;
        }

        public function getAd(){
            return $this->ad;
        }

        public function getDate(){
            return $this->date;
        }

        public function getStatus(){
            return $this->status;
        }

        public function setStatus($val){
            $this->status = $val;
            return $this;
        }

        public function getTasks(){
            return $this->tasks;
        }
    }