<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;


    /**
     * @ORM\Entity
     * @ORM\Table(name="skill")
     */
    class Skill{
        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\Column(type="string")
         */
        private $description;

        /**
         * @ORM\ManyToOne(targetEntity="User", inversedBy="skill")
         * @ORM\JoinColumn(name="user", referencedColumnName="id")
         */
        private $user;

        const name = 'Umiejętności';

        public function getClass(){
            return get_class($this);
        }

        public function getId(){
            return $this->id;
        }

        public function getDescription(){
            return $this->description;
        }

        public function setDescription($val){
            $this->description = $val;
            return $this;
        }

        public function getUser(){
            return $this->user;
        }

        public function setUser($val){
            $this->user = $val;
            return $this;
        }
    }