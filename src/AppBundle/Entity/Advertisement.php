<?php

    namespace AppBundle\Entity;

    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\AdvertisementRepository")
     * @ORM\Table(name="advertisements")
     */
    class Advertisement{

        /**
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         * @ORM\Id
         */
        private $id;

        /**
         * @ORM\Column(type="string", length=150)
         */
        private $job;

        /**
         * @ORM\Column(type="integer")
         */
        private $minsalary;

        /**
         * @ORM\Column(type="integer", nullable=true)
         */
        private $maxsalary;

        /**
         * @ORM\Column(type="string", length=10)
         */
        private $salarytype;

        /**
         * @ORM\Column(type="text")
         */
        private $description;

        /**
         * @ORM\OneToMany(targetEntity="AdRequirement", mappedBy="ad", cascade={"persist", "remove"}, orphanRemoval=true)
         */
        private $requirements;

        /**
         * @ORM\Column(type="string", length=255)
         */
        private $tags;

        /**
         * @ORM\Column(type="datetime")
         */
        private $date;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumn(name="user", referencedColumnName="id")
         */
        private $user;

        /**
         * @ORM\OneToMany(targetEntity="Application", mappedBy="ad", fetch="EXTRA_LAZY")
         * @ORM\OrderBy({"date"="DESC"})
         */
        private $applications;

        public function __construct(){
            $this->requirements = new ArrayCollection();
            $this->applications = new ArrayCollection();
        }

        public function getId(){
            return $this->id;
        }

        public function getJob(){
            return $this->job;
        }

        public function setJob($val){
            $this->job = $val;
            return $this;
        }

        public function getMinSalary(){
            return $this->minsalary;
        }

        public function setMinSalary($val){
            $this->minsalary = $val;
            return $this;
        }

        public function getMaxSalary(){
            return $this->maxsalary;
        }

        public function setMaxSalary($val){
            $this->maxsalary = $val;
            return $this;
        }

        public function getSalaryType(){
            return $this->salarytype;
        }

        public function setSalaryType($val){
            $this->salarytype = $val;
            return $this;
        }

        public function getDescription(){
            return $this->description;
        }

        public function setDescription($val){
            $this->description = $val;
            return $this;
        }

        public function getRequirements(){
            return $this->requirements;
        }

        public function setRequirements($val){
            $this->requirements = $val;
            return $this;
        }

        public function addRequirement(AdRequirement $requirement){
            $requirement->setAd($this);
            $this->requirements->add($requirement);
            return $this;
        }

        public function removeRequirement(AdRequirement $requirement){
            if(!$this->requirements->contains($requirement))
                return;
            $this->requirements->removeElement($requirement);
            $requirement->setAd(null);
        }

        public function getTags(){
            return $this->tags;
        }

        public function setTags($val){
            $this->tags = $val;
            return $this;
        }

        public function getDate(){
            return $this->date;
        }

        public function setDate($val){
            $this->date = $val;
            return $this;
        }

        public function getUser(){
            return $this->user;
        }

        public function setUser($val){
            $this->user = $val;
            return $this;
        }

        public function getApplications(){
            return $this->applications;
        }
    }