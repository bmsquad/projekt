<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
     * @ORM\Table(name="task")
     */
    class Task{

        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\ManyToOne(targetEntity="Application", inversedBy="tasks")
         * @ORM\JoinColumn(name="application", referencedColumnName="id")
         */
        private $application;

        /**
         * @ORM\Column(type="string", length=511)
         */
        private $task;

        /**
         * @ORM\Column(type="text", nullable=true)
         */
        private $answer;

        /**
         * @ORM\Column(type="boolean")
         */
        private $verified = false;

        public function getId(){
            return $this->id;
        }

        public function getApplication(){
            return $this->application;
        }

        public function setApplication($val){
            $this->application = $val;
            return $this;
        }

        public function getTask(){
            return $this->task;
        }

        public function setTask($val){
            $this->task = $val;
            return $this;
        }

        public function getAnswer(){
            return $this->answer;
        }

        public function setAnswer($val){
            $this->answer = $val;
            return $this;
        }

        public function getVerified(){
            return $this->verified;
        }

        public function setVerified($val){
            $this->verified = $val;
            return $this;
        }
    }