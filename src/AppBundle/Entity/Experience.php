<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity
     * @ORM\Table(name="experience")
     */
    class Experience{

        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\Column(type="date")
         */
        private $datefrom;

        /**
         * @ORM\Column(type="date", nullable=true)
         */
        private $dateto;

        /**
         * @ORM\Column(type="string", length=75)
         */
        private $value;

        /**
         * @ORM\Column(type="string", length=255)
         */
        private $description;

        /**
         * @ORM\ManyToOne(targetEntity="User", inversedBy="experience")
         * @ORM\JoinColumn(name="user", referencedColumnName="id")
         */
        private $user;

        const name = 'Doświadczenie';

        public function getClass(){
            return get_class($this);
        }

        public function getId(){
            return $this->id;
        }

        public function getDateFrom(){
            return $this->datefrom;
        }

        public function setDateFrom($val){
            $this->datefrom = $val;
            return $this;
        }

        public function getDateTo(){
            return $this->dateto;
        }

        public function setDateTo($val){
            $this->dateto = $val;
            return $this;
        }

        public function getValue(){
            return $this->value;
        }

        public function setValue($val){
            $this->value = $val;
            return $this;
        }

        public function getDescription(){
            return $this->description;
        }

        public function setDescription($val){
            $this->description = $val;
            return $this;
        }

        public function getUser(){
            return $this->user;
        }

        public function setUser($val){
            $this->user = $val;
            return $this;
        }
    }