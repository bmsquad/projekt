<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;


    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageRepository")
     * @ORM\Table(name="message", indexes={@ORM\Index(name="m_idx", columns={"sender", "receiver"}), @ORM\Index(name="m_seen", columns={"receiver", "seen"})})
     */
    class Message{
        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\Column(type="string")
         */
        private $value;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumn(name="sender", referencedColumnName="id")
         */
        private $sender;

        /**
         * @ORM\ManyToOne(targetEntity="User")
         * @ORM\JoinColumn(name="receiver", referencedColumnName="id")
         */
        private $receiver;

        /**
         * @ORM\Column(type="boolean")
         */
        private $seen = false;

        /**
         * @ORM\Column(type="datetime")
         */
        private $date;

        public function getId(){
            return $this->id;
        }

        public function getValue(){
            return $this->value;
        }

        public function setValue($val){
            $this->value = htmlspecialchars(trim($val));
            return $this;
        }

        public function getSender(){
            return $this->sender;
        }

        public function setSender($val){
            $this->sender = $val;
            return $this;
        }

        public function getReceiver(){
            return $this->receiver;
        }

        public function setReceiver($val){
            $this->receiver = $val;
            return $this;
        }

        public function getSeen(){
            return $this->seen;
        }

        public function setSeen($val){
            $this->seen = $val;
            return $this;
        }

        public function getDate(){
            return $this->date;
        }

        public function setDate($val){
            $this->date = $val;
            return $this;
        }

        public function __construct()
        {
            $this->date = new \DateTime();
        }
    }