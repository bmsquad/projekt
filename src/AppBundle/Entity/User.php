<?php
    namespace AppBundle\Entity;

    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Security\Core\User\AdvancedUserInterface;
    use Symfony\Component\Validator\Constraints as Assert;
    use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

    /**
     * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
     * @ORM\Table(name="users")
     * @UniqueEntity(
     *     fields={"mail"},
     *     message="Podany adres email jest już w użyciu"
     * )
     */
    class User implements AdvancedUserInterface, \Serializable{

        /**
         * @ORM\Column(type="integer")
         * @ORM\Id
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        private $id;

        /**
         * @ORM\Column(type="string", length=255)
         */
        private $mail;

        /**
         * @ORM\Column(type="string", length=255)
         */
        private $password;

        /**
         * @ORM\Column(type="string", length=30)
         */
        private $role;

        /**
         * @ORM\Column(type="string", length=20)
         */
        private $name;

        /**
         * @ORM\Column(type="string", length=30)
         */
        private $surname;

        /**
         * @ORM\Column(type="string", length=50, nullable=true)
         */
        private $address;

        /**
         * @ORM\Column(type="string", length=15, nullable=true)
         */
        private $number;

        /**
         * @ORM\Column(type="date", nullable=true)
         */
        private $birthdate;

        /**
         * @ORM\Column(type="string", length=255, nullable=true)
         * @Assert\File(
         *     mimeTypes={"image/gif", "image/png", "image/jpeg"},
         *     mimeTypesMessage="Zdjęcie musi być plikiem graficznym z rozszerzeniem .gif, .png lub .jpg",
         *     maxSize="512k",
         *     maxSizeMessage="Maksymalny rozmiar pliku to 512kB"
         * )
         */
        private $photo;

        /**
         * @ORM\OneToMany(targetEntity="Experience", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
         * @ORM\OrderBy({"dateto"="DESC"})
         */
        private $experience;

        /**
         * @ORM\OneToMany(targetEntity="Education", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
         * @ORM\OrderBy({"dateto"="DESC"})
         */
        private $education;

        /**
         * @ORM\OneToMany(targetEntity="Language", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
         */
        private $language;

        /**
         * @ORM\OneToMany(targetEntity="Skill", mappedBy="user", cascade={"persist"}, orphanRemoval=true)
         */
        private $skill;

        /**
         * @ORM\OneToMany(targetEntity="Company", mappedBy="user")
         */
        private $company;

        public function __construct(){
            $this->experience = new ArrayCollection();
            $this->education = new ArrayCollection();
            $this->language = new ArrayCollection();
            $this->skill = new ArrayCollection();
        }

        public function getId(){
            return $this->id;
        }

        public function getMail(){
            return $this->mail;
        }

        public function setMail($val){
            $this->mail = $val;
            return $this;
        }

        public function getPassword(){
            return $this->password;
        }

        public function setPassword($val){
            $this->password = $val;
            return $this;
        }

        public function getRole(){
            return $this->role;
        }

        public function setRole($val){
            $this->role = $val;
            return $this;
        }

        public function getName(){
            return $this->name;
        }

        public function setName($val){
            $this->name = $val;
            return $this;
        }

        public function getSurname(){
            return $this->surname;
        }

        public function setSurname($val){
            $this->surname = $val;
            return $this;
        }

        public function getFullname(){
            return $this->name . ' ' . $this->surname;
        }

        public function getAddress(){
            return $this->address;
        }

        public function setAddress($val){
            $this->address = $val;
            return $this;
        }

        public function getNumber(){
            return $this->number;
        }

        public function setNumber($val){
            $this->number = $val;
            return $this;
        }

        public function getBirthDate(){
            return $this->birthdate;
        }

        public function setBirthDate($val){
            $this->birthdate = $val;
            return $this;
        }

        public function getPhoto(){
            return $this->photo;
        }

        public function setPhoto($val){
            $this->photo = $val;
            return $this;
        }

        public function getExperience(){

            return $this->experience;
        }

        public function setExperience($val){
            $this->experience = $val;
            return $this;
        }

        public function addExperience(Experience $experience){
            $experience->setUser($this);
            $this->experience->add($experience);
            return $this;
        }

        public function removeExperience(Experience $experience){
            if(!$this->experience->contains($experience))
                return;
            $this->experience->removeElement($experience);
            $experience->setUser(null);
        }

        public function getEducation(){
            return $this->education;
        }

        public function setEducation($val){
            $this->education = $val;
            return $this;
        }

        public function addEducation(Education $education){
            $education->setUser($this);
            $this->education->add($education);
            return $this;
        }

        public function removeEducation(Education $education){
            if(!$this->education->contains($education))
                return;
            $this->education->removeElement($education);
            $education->setUser(null);
        }

        public function getLanguage(){
            return $this->language;
        }

        public function setLanguage($val){
            $this->language = $val;
            return $this;
        }

        public function addLanguage(Language $language){
            $language->setUser($this);
            $this->language->add($language);
            return $this;
        }

        public function removeLanguage(Language $language){
            if (!$this->language->contains($language))
                return;
            $this->language->removeElement($language);
            $language->setUser(null);
        }

        public function getSkill(){
            return $this->skill;
        }

        public function setSkill($val){
            $this->skill = $val;
            return $this;
        }

        public function addSkill(Skill $skill){
            $skill->setUser($this);
            $this->skill->add($skill);
            return $this;
        }

        public function removeSkill(Skill $skill){
            if (!$this->skill->contains($skill))
                return;
            $this->skill->removeElement($skill);
            $skill->setUser(null);
        }

        public function getCompany(){
            return $this->company[0];
        }

        public function setCompany($val){
            $this->company = $val;
            return $this;
        }

        public function isAccountNonExpired(){
            return true;
        }

        public function isAccountNonLocked(){
            return true;
        }

        public function isCredentialsNonExpired(){
            return true;
        }

        public function isEnabled(){
            return true;
        }

        public function getUsername(){
            return $this->mail;
        }

        public function getRoles(){
            return array($this->role);
        }

        public function getSalt(){
            return null;
        }

        public function serialize(){
            return serialize(array(
                $this->id,
                $this->mail,
                $this->password
            ));
        }

        public function unserialize($serialized){
            list(
                $this->id,
                $this->mail,
                $this->password
                ) = unserialize($serialized);
        }

        public function eraseCredentials(){
            return null;
        }
    }