<?php
    namespace AppBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;

    /**
     * @ORM\Entity
     * @ORM\Table(name="ad_requirements", indexes={@ORM\Index(name="ar_ad", columns={"ad"})})
     */
    class AdRequirement{

        /**
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         * @ORM\Id
         */
        private $id;

        /**
         * @ORM\ManyToOne(targetEntity="Advertisement", inversedBy="requirements")
         * @ORM\JoinColumn(name="ad", referencedColumnName="id")
         */
        private $ad;

        /**
         * @ORM\Column(type="string")
         */
        private $value;

        public function getId(){
            return $this->id;
        }

        public function getAd(){
            return $this->ad;
        }

        public function setAd($val){
            $this->ad = $val;
            return $this;
        }

        public function getValue(){
            return $this->value;
        }

        public function setValue($val){
            $this->value = $val;
            return $this;
        }
    }