<?php

    namespace AppBundle\Serializer;

    class MessageSerializer extends CustomSerializer{

        public function __construct(){
            parent::__construct();
            $this->normalizer->setCallbacks(array('sender' => $this->userCallback, 'receiver' => $this->userCallback, 'date' => $this->dateCallback));
        }
    }