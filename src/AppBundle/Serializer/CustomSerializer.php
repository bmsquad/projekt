<?php
    namespace AppBundle\Serializer;

    use AppBundle\Entity\User;
    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
    use Symfony\Component\Serializer\Serializer;

    class CustomSerializer{

        protected $serializer;

        protected $normalizer;

        protected $encoder;

        protected $userCallback;

        protected $dateCallback;

        public function __construct(){
            $this->normalizer = new ObjectNormalizer();
            $this->encoder = new JsonEncoder();
            $this->userCallback = function($user){
                return $user instanceof User ? $user->getId() : '';
            };
            $this->dateCallback = function($date){
                return $date instanceof \DateTime ? $date->format('H:i:s d-m-Y') : '';
            };
        }

        public function getDateCallback(){
            return $this->dateCallback;
        }

        public function getUserCallback(){
            return $this->userCallback;
        }

        public function setCallbacks(array $callbacks){
            $this->normalizer->setCallbacks($callbacks);
        }

        public function serialize($obj, $attr){
            if($this->serializer === null)
                $this->serializer = new Serializer(array($this->normalizer), array($this->encoder));
            return $this->serializer->serialize($obj, 'json', array('attributes' => $attr));
        }
    }