<?php
    namespace AppBundle\Serializer;

    use Symfony\Component\Serializer\Encoder\JsonEncoder;
    use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
    use Symfony\Component\Serializer\Serializer;

    class UserSerializer{

        private $serializer;

        public function __construct(){
            $normalizer = new ObjectNormalizer();
            $encoder = new JsonEncoder();
            $this->serializer = new Serializer(array($normalizer), array($encoder));
        }

        public function serialize($obj, $attr){
            return $this->serializer->serialize($obj, 'json', array('attributes' => $attr));
        }
    }