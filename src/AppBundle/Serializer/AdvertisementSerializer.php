<?php
    namespace AppBundle\Serializer;

    class AdvertisementSerializer extends CustomSerializer{

        public function __construct(){
            parent::__construct();
            $this->normalizer->setCallbacks(array('user' => $this->userCallback, 'date' => $this->dateCallback));
        }
    }