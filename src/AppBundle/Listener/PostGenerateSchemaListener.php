<?php
    namespace AppBundle\Listener;

    use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;

    class PostGenerateSchemaListener{

        private $tables = array('last_msg');

        public function postGenerateSchema(GenerateSchemaEventArgs $args){
            $schema = $args->getSchema();
            $db = $args->getEntityManager()->getConnection()->getDatabase();

            foreach($this->tables as &$table){
                if(strpos($table, $db) === false)
                    $table = "$db.$table";
            }

            foreach($schema->getTableNames() as $tableName){
                if(in_array($tableName, $this->tables))
                    $schema->dropTable($tableName);
            }

        }
    }