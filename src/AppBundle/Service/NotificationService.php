<?php
    namespace AppBundle\Service;

    use AppBundle\Entity\Notification;
    use AppBundle\Serializer\NotificationSerializer;
    use Doctrine\ORM\EntityManagerInterface;

    class NotificationService{

        private $em;

        private $serializer;

        public function __construct(EntityManagerInterface $em, NotificationSerializer $serializer){
            $this->em = $em;
            $this->serializer = $serializer;
        }

        public function getLastNotifications($user, $limit = 5, $amount = 0){
            return $this->em->getRepository(Notification::class)->getLastNotifications($user, $limit, $amount);
        }

        public function getAmount($user, $limit, $amount){
            $notifications = $this->getLastNotifications($user, $limit, $amount);
            return $this->serializer->serialize($notifications, array('id', 'value', 'date', 'seen'));
        }

        public function setSeen($notification, $value){
            if(!$notification)
                return false;
            return $this->em->getRepository(Notification::class)->setSeen($notification, $value);
        }
    }