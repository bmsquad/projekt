<?php
    namespace AppBundle\Service;

    use AppBundle\Entity\Message;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
    use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

    class MessageService{

        private $token;
        private $em;

        public function __construct(TokenStorageInterface $token, EntityManagerInterface $em){
            $this->token = $token;
            $this->em = $em;
        }

        public function hasUnseen(){
            $token = $this->token->getToken();
            if(!$token || !$user = $token->getUser())
                return false;

            return $this->em->createQueryBuilder()
                ->select('count(m.id)')
                ->from(Message::class, 'm')
                ->where('m.receiver = :user')
                ->andWhere('m.seen = false')
                ->setParameter('user', $user)
                ->getQuery()
                ->getSingleScalarResult();
        }
    }