<?php
    namespace AppBundle\Service;

    use Symfony\Component\HttpFoundation\File\Exception\FileException;
    use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\HttpFoundation\File\UploadedFile;

    class PhotoService{

        public function fileHandler($file, &$user){
            if($file instanceof UploadedFile){
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                try{
                    $file->move(
                        './avatars/',
                        $fileName
                    );
                }catch(FileException $e){
                    echo 'ta gówno';
                }
                $user->setPhoto($fileName);
            }elseif($file instanceof File){
                $user->setPhoto($file->getFilename());
            }
        }

        public function setOldFile(&$user){
            $oldFile = $user->getPhoto();
            if($oldFile){
                try{
                    $f = new File('./avatars/' . $user->getPhoto());
                    $user->setPhoto($f);
                    $oldFile = $f;
                }catch(FileNotFoundException $e){
                    $user->setPhoto(null);
                }
            }
            return $oldFile;
        }
    }
