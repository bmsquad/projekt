<?php
    namespace AppBundle\Service;

    use AppBundle\Entity\Advertisement;
    use AppBundle\Entity\Application;
    use Doctrine\ORM\EntityManagerInterface;

    class AdvertisementService{

        private $em;

        public function __construct(EntityManagerInterface $em){
            $this->em = $em;
        }

        public function getRealUserCount($ad){
            return $this->em->createQueryBuilder()
                ->select('count(distinct ap.user)')
                ->from(Application::class, 'ap')
                ->where('ap.ad = :ad')
                ->setParameter('ad', $ad instanceof Advertisement ? $ad->getId() : $ad)
                ->getQuery()
                ->getSingleScalarResult();
        }
    }