<?php
    namespace AppBundle\Security;

    use AppBundle\Entity\Task;
    use AppBundle\Entity\User;
    use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
    use Symfony\Component\Security\Core\Authorization\Voter\Voter;

    class TaskVoter extends Voter{

        public function supports($attribute, $subject){
            if(!in_array($attribute, array('fill', 'review'))){
                return false;
            }
            if(!$subject instanceof Task){
                return false;
            }
            return true;
        }

        public function voteOnAttribute($attribute, $subject, TokenInterface $token){
            $user = $token->getUser();
            if(!$user instanceof User)
                return false;

            $task = $subject;

            switch($attribute){
                case 'fill':
                    return $this->canFill($task, $user);
                case 'review':
                    return $this->canReview($task, $user);
            }

            throw new \LogicException('This code should not be reached!');
        }

        public function canFill(Task $task, User $user){
            return $task->getApplication()->getUser() === $user;
        }

        public function canReview(Task $task, User $user){
            return $task->getApplication()->getAd()->getUser() === $user;
        }
    }