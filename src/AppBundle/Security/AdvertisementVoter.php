<?php
    namespace AppBundle\Security;

    use AppBundle\Entity\Advertisement;
    use AppBundle\Entity\Application;
    use AppBundle\Entity\User;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
    use Symfony\Component\Security\Core\Authorization\Voter\Voter;

    class AdvertisementVoter extends Voter{

        private $em;

        public function __construct(EntityManagerInterface $em){
            $this->em = $em;
        }

        protected function supports($attribute, $subject){
            if(!in_array($attribute, array('edit', 'application'))){
                return false;
            }
            if(!$subject instanceof Advertisement){
                return false;
            }
            return true;
        }

        protected function voteOnAttribute($attribute, $subject, TokenInterface $token){
            $user = $token->getUser();
            if(!$user instanceof User){
                return false;
            }
            $ad = $subject;

            switch($attribute){
                case 'edit':
                    return $this->canEdit($ad, $user);
                case 'application':
                    return $this->canApplicate($ad, $user);
            }

            throw new \LogicException('This code should not be reached!');
        }

        private function canEdit(Advertisement $ad, User $user){
            return $user === $ad->getUser();
        }

        private function canApplicate(Advertisement $ad, User $user){
            return $user->getRole() === 'ROLE_EMPLOYEE' && !$this->em->getRepository(Application::class)->findOneBy(array('user' => $user, 'ad' => $ad, 'status' => array(0, 1, 3, 4)));
        }
    }