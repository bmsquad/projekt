<?php
    namespace AppBundle\Security;

    use AppBundle\Entity\Application;
    use AppBundle\Entity\User;
    use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
    use Symfony\Component\Security\Core\Authorization\Voter\Voter;

    class ApplicationVoter extends Voter{

        protected function supports($attribute, $subject){
            if(!in_array($attribute, array('task')))
                return false;

            if(!$subject instanceof Application)
                return false;

            return true;
        }

        protected function voteOnAttribute($attribute, $subject, TokenInterface $token){
            $user = $token->getUser();
            if(!$user instanceof User)
                return false;

            $application = $subject;

            switch($attribute){
                case 'task':
                    return $this->canTask($application, $user);
            }

            throw new \LogicException('This code should not be reached!');
        }

        private function canTask(Application $application, User $user){
            return $application->getAd()->getUser() === $user;
        }
    }