<?php
    namespace AppBundle\Command;

    use Doctrine\DBAL\Exception\ConnectionException;
    use Doctrine\DBAL\Exception\DriverException;
    use Doctrine\DBAL\Exception\TableExistsException;
    use Doctrine\ORM\EntityManagerInterface;
    use Symfony\Component\Console\Command\Command;
    use Symfony\Component\Console\Input\ArrayInput;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;

    class DatabaseInitCommand extends Command{

        private $em;

        public function __construct(EntityManagerInterface $em){
            $this->em = $em;
            parent::__construct();
        }

        protected function configure(){
            $this->setName('app:database-init')
                ->setDescription('Database init');
        }

        protected function execute(InputInterface $input, OutputInterface $output){
            try{
                $conn = $this->em->getConnection();
                try{
                    $conn->connect();
                }catch(ConnectionException $e){
                    if($e->getErrorCode() === 1049){
                        $command = $this->getApplication()->find('doctrine:database:create');
                        $command->run($input, $output);
                    }else
                        throw $e;
                }

                $command = $this->getApplication()->find('doctrine:schema:update');
                $command->run(new ArrayInput(array('--force' => true)), $output);

                try{
                    $conn->query('CREATE VIEW `last_msg` AS SELECT * from message m where m.id in (SELECT max(id) as i from message m group by if(sender < receiver, concat(sender, " ", receiver), concat(receiver, " ", sender))) order by m.date desc');
                    $output->writeln('View created');
                }catch(TableExistsException $e){
                    $output->writeln('View already exists');
                }

                try{
                    $conn->query('CREATE TABLE `stats` (`users` int(10) UNSIGNED NOT NULL DEFAULT \'0\', `ads` int(10) UNSIGNED NOT NULL DEFAULT \'0\', `applications` int(10) UNSIGNED NOT NULL DEFAULT \'0\') ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;');
                    $conn->query('INSERT INTO `stats` (`users`, `ads`, `applications`) VALUES (\'0\', \'0\', \'0\');');
                    $output->writeln('Table stats created');
                }catch(TableExistsException $e){
                    $output->writeln('Table stats already exists');
                }

                $triggers = array(
                    'stats_users' => 'CREATE TRIGGER `stats_users` AFTER INSERT ON `users` FOR EACH ROW update stats set users = users + 1',
                    'stats_ads' => 'CREATE TRIGGER `stats_ads` AFTER INSERT ON `advertisements` FOR EACH ROW update stats set ads = ads + 1',
                    'stats_applications' => 'CREATE TRIGGER `stats_applications` AFTER INSERT ON `applications` FOR EACH ROW update stats set applications = applications + 1'
                );

                foreach($triggers as $trigger => $sql){
                    try{
                        $conn->query($sql);
                        $output->writeln("Trigger $trigger created");
                    }catch(DriverException $e){
                        if($e->getErrorCode() === 1359)
                            $output->writeln("Trigger $trigger already exists");
                        else
                            throw $e;
                    }
                }

                $output->writeln('Database initiated');
            }catch(\Exception $e){
                $output->writeln($e->getMessage());
            }
        }
    }