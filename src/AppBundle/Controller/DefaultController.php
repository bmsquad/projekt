<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Advertisement;
use AppBundle\Entity\Application;
use AppBundle\Entity\LastMessageView;
use AppBundle\Entity\User;
use AppBundle\Form\CvType;
use AppBundle\Form\EmployerType;
use AppBundle\Form\UserType;
use AppBundle\Serializer\CustomSerializer;
use AppBundle\Serializer\UserSerializer;
use AppBundle\Service\AdvertisementService;
use AppBundle\Service\PhotoService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request){
        if($this->get('security.authorization_checker')->isGranted(['ROLE_EMPLOYEE', 'ROLE_EMPLOYER']))
            return $this->redirectToRoute('home');
        $em = $this->getDoctrine()->getManager();
        $stats = $this->getDoctrine()->getConnection()->query('select * from stats')->fetch();
        $employeeForm = $this->get('form.factory')->createNamed('employee', UserType::class);
        $employerForm = $this->get('form.factory')->createNamed('employer', UserType::class);
        $employeeForm->handleRequest($request);
        $employerForm->handleRequest($request);
        if(($employeeForm->isSubmitted() && $employeeForm->isValid() || ($employerForm->isSubmitted() && $employerForm->isValid()))){
            $data = $employeeForm->getData() ?: $employerForm->getData();
            $em->persist($data);
            $em->flush();
            $this->addFlash(
                'register',
                'Rejestracja zakończona pomyślnie'
            );
            return $this->redirect($this->generateUrl('homepage') . '#login');
        }
        return $this->render('default/index.html.twig', array(
            'employeeForm' => $employeeForm->createView(),
            'employerForm' => $employerForm->createView(),
            'stats' => $stats
        ));
    }

    /**
     * @Route("/home", name="home")
     */
    public function homeAction(AdvertisementService $adService){
        if($this->isGranted('ROLE_EMPLOYEE'))
            return $this->homeEmployeeAction();
        else
            return $this->homeEmployerAction($adService);
    }

    public function homeEmployeeAction(){
        $ads = $this->getDoctrine()->getRepository(Advertisement::class)->getNewest();
        return $this->render('default/home.html.twig', array(
            'ads' => $ads,
        ));
    }

    public function homeEmployerAction(AdvertisementService $adService){
        $applications = $this->getDoctrine()->getRepository(Application::class)->getNewest($this->getUser());
        $arr = array();
        foreach($applications as $app){
            $arr[$app->getAd()->getId()][] = $app;
        }
        return $this->render('default/home.html.twig', array(
            'app_groups' => $arr,
            'adService' => $adService
        ));
    }

    /**
     * @Route("/profile", name="profile")
     */
    public function profileAction(Request $request, SessionInterface $session, PhotoService $photoService){
        $em = $this->getDoctrine()->getManager();
        if($this->isGranted('ROLE_EMPLOYEE')){
            $user = $em->getRepository(User::class)->getWithJoins($this->getUser());
            $oldFile = $photoService->setOldFile($user);
            $form = $this->createForm(CvType::class, $user);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $file = $user->getPhoto() ?: $oldFile;
                $photoService->fileHandler($file, $user);
                $em->persist($user);
                $em->flush();
                return $this->redirect($request->getRequestUri());
            }
            return $this->render('default/profile.html.twig', array(
                'form' => $form->createView()
            ));
        }elseif($this->isGranted('ROLE_EMPLOYER')){
            $user = $em->getRepository(User::class)->getWithJoins($this->getUser(), true);
            $oldFile = $photoService->setOldFile($user);
            $form = $this->createForm(EmployerType::class, $user);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $remove = $form['photo-remove']->getData();
                if($remove){
                    if($oldFile){
                        $user->setPhoto(null);
                        $filesystem = new Filesystem();
                        $filesystem->remove($oldFile->getPathname());
                    }
                }else{
                    $file = $user->getPhoto() ?: $oldFile;
                    $photoService->fileHandler($file, $user);
                }
                $em->persist($user);
                $em->flush();
                return $this->redirect($request->getRequestUri());
            }
            return $this->render('default/profile_employer.html.twig', array(
                'form' => $form->createView()
            ));
        }
    }

    /**
     * @Route("/profile/{id}", name="profile_show")
     */
    public function profileShowAction($id){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);
        if(!$user)
            throw new NotFoundHttpException();
        if($user->getRole()==='ROLE_EMPLOYER'){
            return $this->render('profile/profile_employer_show.html.twig', array(
                'user' => $user,
            ));
        }else{
            return $this->render('profile/profile_employee_show.html.twig', array(
                'user' => $user,
            ));
        }
    }

    /**
     * @Route("/user/search", name="user_search")
     */
    public function userSearchAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $receiver = $request->request->get('value');
        $users = $em->getRepository(User::class)->search($receiver, $this->getUser());
        if($request->isXmlHttpRequest() && $receiver){
            $serializer = new UserSerializer();
            return JsonResponse::fromJsonString($serializer->serialize($users, ['id','name','surname','photo']));
        }
        throw new BadRequestHttpException();
    }

    /**
     * @Route("/user/data", name="user_data")
     */
    public function userDataAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        if(!$request->isXmlHttpRequest() || !$user = $request->request->get('user'))
            throw new BadRequestHttpException();

        $user = $em->getRepository(User::class)->getWithJoins($user);
        return $this->render('ads/user_data.html.twig', array(
            'user' => $user
        ));
    }
}
