<?php
namespace AppBundle\Controller;

use AppBundle\Service\NotificationService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class NotificationsController extends Controller{

    private $notification;

    public function __construct(NotificationService $notification){
        $this->notification = $notification;
    }

    /**
     * @Route("/notifications", name="notifications")
     */
    public function notificationsIndexAction(){
        $notifications = $this->notification->getLastNotifications($this->getUser(), 10, 0);
        return $this->render('notifications/index.html.twig', array(
            'notifications' => $notifications
        ));
    }

    /**
     * @Route("/notifications/ajax", name="notifications_ajax", methods={"POST"})
     */
    public function notificationsAjaxAction(Request $request){
        if($request->query->get('seen'))
            return new JsonResponse($this->notification->setSeen($request->request->get('notification'), $request->request->get('value')));

        if($request->request->get('amount') || $request->request->get('quantity'))
            return JsonResponse::fromJsonString($this->notification->getAmount($this->getUser(), $request->request->get('quantity'), $request->request->get('amount')));
        throw new BadRequestHttpException();
    }
}