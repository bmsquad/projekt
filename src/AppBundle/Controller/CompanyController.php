<?php
    namespace AppBundle\Controller;

    use AppBundle\Entity\Company;
    use NIP24\NIP24Client;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Session\SessionInterface;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
    use Symfony\Component\Routing\Annotation\Route;

    /**
     * @Route(methods={"POST"})
     */
    class CompanyController extends Controller{

        /**
         * @Route("/company/check", name="company_check")
         */
        public function checkAction(Request $request, SessionInterface $session){
            if($nip = $request->request->get('nip')){
                $nip24 = new NIP24Client(/*$this->getParameter('id'), $this->getParameter('key')*/);
                $company = $nip24->getInvoiceData($nip);
                if($company){
                    $session->set('company', $company);
                }
                return new JsonResponse($company);
            }
            throw new BadRequestHttpException();
        }

        /**
         * @Route("/company/set", name="company_set")
         */
        public function setAction(Request $request, SessionInterface $session){
            if($cnip = $request->request->get('company')){
                $em = $this->getDoctrine()->getManager();
                $company = $session->get('company');
                $session->remove('company');
                if($cnip !== $company->nip){
                    $nip24 = new NIP24Client(/*$this->getParameter('id'), $this->getParameter('key')*/);
                    $company = $nip24->getInvoiceData($cnip);
                }
                if($company){
                    $companyObj = new Company($company, $this->getUser());
                    $em->persist($companyObj);
                    $em->flush();
                    return new JsonResponse(true);
                }
            }
            throw new BadRequestHttpException();
        }

        /**
         * @Route("/company/remove", name="company_remove")
         */
        public function removeAction(Request $request){
            $company = $request->request->get('company', 0);
            $user = $request->request->get('user', 0);
            if($company && $user){
                $em = $this->getDoctrine()->getManager();
                $company = $em->getRepository(Company::class)->findOneBy(array('id' => $company, 'user' => $user));
                if($company){
                    $em->remove($company);
                    $em->flush();
                    return $this->render('company/search.html.twig');
                }
            }
            throw new BadRequestHttpException();
        }

        /**
         * @Route("/company/refresh", name="company_refresh")
         */
        public function refreshAction(Request $request){
            $company = $request->request->get('company', 0);
            $user = $request->request->get('user', 0);
            if($company && $user){
                $em = $this->getDoctrine()->getManager();
                $company = $em->getRepository(Company::class)->findOneBy(array('id' => $company, 'user' => $user));
                if($company){
                    $nip24 = new NIP24Client(/*$this->getParameter('id'), $this->getParameter('key')*/);
                    $c = $nip24->getInvoiceData($company->getNip());
                    if($c){
                        $company->createFromApiData($c, $this->getUser());
                        $em->persist($company);
                    }else
                        $em->remove($company);
                    $em->flush();
                    if(!$company)
                        return $this->redirectToRoute('profile');
                    return $this->render('company/data.html.twig', array(
                        'company' => $company
                    ));
                }
            }
            throw new BadRequestHttpException();
        }
    }