<?php
    namespace AppBundle\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\Routing\Annotation\Route;
    use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

    class SecurityController extends Controller{

        /**
         * @Route("/login", name="login")
         */
        public function loginAction(AuthenticationUtils $utils){
            $error = $utils->getLastAuthenticationError();
            $username = $utils->getLastUsername();
            $this->addFlash('login', $error);
            $this->addFlash('login', $username);
            return $this->redirect($this->generateUrl('homepage') . '#login');
        }
    }