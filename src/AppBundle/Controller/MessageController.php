<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Message;
use AppBundle\Form\MessageType;
use AppBundle\Serializer\MessageSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends Controller
{
    /**
     * @Route("/message/search", name="message_search")
     */
    public function ajaxAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $receiver = $request->request->get('user');
        $users = $em->getRepository(User::class)->search($receiver, $this->getUser());
        if($request->isXmlHttpRequest()){
            $JsonData = array();
            foreach($users as $user){
                $JsonData[] = array(
                    'id' => $user->getId(),
                    'name' => $user->getName(),
                    'surname' => $user->getSurname(),
                    'photo' => $user->getPhoto(),
                );
            }
            return new JsonResponse($JsonData);
        }
        return new Response();
    }

    /**
     * @Route("/message/page", name="message_page")
     */
    public function pageAction(Request $request, MessageSerializer $serializer){
        if($request->isXmlHttpRequest()){
            $em = $this->getDoctrine()->getManager();
            $page = $request->request->get('page');
            $receiver = $request->request->get('receiver');
            $receiverOb = $em->getRepository(User::class)->find($receiver);
            if(!$receiverOb)
                throw new BadRequestHttpException();
            $messages = $em->getRepository(Message::class)->getMessages($this->getUser()->getId(), $receiver, $page);
            if($request->request->get('form')){
                $form = $this->createForm(MessageType::class);
                return $this->render('message/messages.html.twig', array(
                    'messages' => $messages,
                    'receiver' => $receiverOb,
                    'form' => $form->createView()
                ));
            }
            return JsonResponse::fromJsonString($serializer->serialize($messages, array('id', 'date', 'receiver', 'sender', 'value')));
        }
        throw new BadRequestHttpException();
    }

    /**
     * @Route("/message/{receiver}", name="message", defaults={"receiver"=null})
     */
    public function messageAction(Request $request, MessageSerializer $serializer, $receiver){
        $em = $this->getDoctrine()->getManager();

        $lm = $em->getRepository(Message::class)->getAllLastMessages($this->getUser());
//        dump($lm);
//        return new Response('<html><body></body></html>');

        if($receiver){
            $receiverOb = $em->getReference(User::class, $receiver);
            if($receiverOb){
                $sender = $this->getUser()->getId();
                $messages = $em->getRepository(Message::class)->getMessages($sender,$receiver);
                $em->createQueryBuilder()
                    ->update(Message::class, 'm')
                    ->set('m.seen', 1)
                    ->where('m.receiver = :user')
                    ->setParameter('user', $this->getUser())
                    ->andWhere('m.seen = 0')
                    ->getQuery()
                    ->execute();
                $form = $this->createForm(MessageType::class);
                $form->handleRequest($request);
                if($form->isSubmitted() && $form->isValid()){
                    $data = $form->getData();
                    if($data->getValue()){
                        $data->setReceiver($receiverOb);
                        //dump($data); die();
                        $em->persist($data);
                        $em->flush();
                        $messages = $em->getRepository(Message::class)->getNewMessages($this->getUser()->getId(), $receiver, $request->request->get('id'));
                        return JsonResponse::fromJsonString($serializer->serialize($messages, array('id', 'sender', 'receiver', 'date', 'value')));
                        //return $this->redirect($request->getRequestUri());
                    }
                }
            }else{
                throw new BadRequestHttpException();
            }
        }
        return $this->render('message/index.html.twig', array(
            'form' => isset($form) ? $form->createView() : null,
            'messages' => $messages ?? null,
            'receiver' => $receiverOb ?? null,
            'lastMessages' => $lm
        ));
    }
}
