<?php
    namespace AppBundle\Controller;

    use AppBundle\Entity\Advertisement;
    use AppBundle\Entity\Application;
    use AppBundle\Entity\Notification;
    use AppBundle\Entity\Task;
    use AppBundle\Entity\User;
    use AppBundle\Form\AdvertisementType;
    use AppBundle\Form\TaskFillType;
    use AppBundle\Form\TaskType;
    use AppBundle\Security\AdvertisementVoter;
    use AppBundle\Serializer\AdvertisementSerializer;
    use AppBundle\Service\AdvertisementService;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
    use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
    use Symfony\Component\Routing\Annotation\Route;

    class AdvertisementController extends Controller{

        /**
         * @Route("/my_advertisements", name="my_ads")
         */
        public function myAdsAction(AdvertisementService $adService){
            if($this->isGranted('ROLE_EMPLOYEE')){
                $applications = $this->getDoctrine()->getRepository(Application::class)->getPage($this->getUser(), 10, 1);
                return $this->render('ads/my_ads.html.twig', array(
                    'applications' => $applications
                ));
            }else{
                $ads = $this->getDoctrine()->getRepository(Advertisement::class)->findBy(array('user' => $this->getUser()));
                return $this->render('/ads/my_ads.html.twig', array(
                    'ads' => $ads,
                    'adService' => $adService
                ));
            }

        }

        /**
         * @Route("/advertisement/add", name="advertisement_add")
         */
        public function addAction(Request $request){
            if(!$this->getUser()->getCompany())
                return $this->redirectToRoute('my_ads');
            $form = $this->createForm(AdvertisementType::class);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $ad = $form->getData();
                $ad->setUser($this->getUser())->setDate(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($ad);
                $em->flush();
                return $this->redirectToRoute('advertisement', array('id' => $ad->getId()));

            }
            return $this->render('/ads/add.html.twig', array(
                'form' => $form->createView()
            ));
        }

        /**
         * @Route("/advertisement/{id}", defaults={"_fragment" = ""}, name="advertisement", requirements={"id"="\d+"})
         */
        public function advertisementAction($id){
            $ad = $this->getDoctrine()->getRepository(Advertisement::class)->getWithRequirements($id);
            if(!$ad)
                throw new NotFoundHttpException();
            if($this->isGranted('ROLE_EMPLOYEE'))
                $hasApplicated = $this->getDoctrine()->getRepository(Application::class)->hasApplicated($this->getUser(), $ad);
            return $this->render('/ads/advertisement.html.twig', array(
                'ad' => $ad,
                'hasApplicated' => $hasApplicated ?? false
            ));
        }

        /**
         * @Route("/advertisement/{id}/edit", name="advertisement_edit")
         */
        public function editAction(Request $request, $id){
            $em = $this->getDoctrine()->getManager();
            $ad = $em->getRepository(Advertisement::class)->find($id);
            if(!$ad || !$this->isGranted('edit', $ad))
                throw new NotFoundHttpException();
            $form = $this->createForm(AdvertisementType::class, $ad);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em->persist($ad);
                $em->flush();
                $this->addFlash('success', 'Zmiany zapisane pomyślnie');
                return $this->redirect($request->getRequestUri());
            }
            return $this->render('/ads/add.html.twig', array(
                'form' => $form->createView()
            ));
        }

        /**
         * @Route("/advertisement/{id}/users", name="advertisement_users")
         */
        public function usersAction($id){
            $em = $this->getDoctrine()->getManager();
            $ad = $em->getRepository(Advertisement::class)->getWithApplications($id);
            if(!$ad)
                throw new NotFoundHttpException();

            if(!$this->isGranted('edit', $ad))
                throw new AccessDeniedHttpException();
            return $this->render('ads/users.html.twig', array(
                'ad' => $ad
            ));
        }

        /**
         * @Route("/advertisement/{id}/applicate", name="applicate", methods={"POST"})
         */
        public function applicateAction(Request $request, $id){
            if(!$request->isXmlHttpRequest())
                throw new BadRequestHttpException();

            $em = $this->getDoctrine()->getManager();
            $ad = $em->getRepository(Advertisement::class)->find($id);
            if(!$ad)
                throw new NotFoundHttpException();
            $user = $this->getUser();
            if($em->getRepository(Application::class)->findOneBy(array('ad' => $ad, 'user' => $user, 'status' => array(0, 1, 3))))
                throw new BadRequestHttpException();
            $application = new Application($user, $ad);
            $notification = new Notification($ad->getUser(),
                $user->getFullname() . ' złożył aplikację do ogłoszenia ' . $ad->getJob(),
                $this->generateUrl('advertisement_users', array('id' => $ad->getId())) . '#' . $user->getId());
            $em->persist($application);
            $em->persist($notification);
            $em->flush();
            return new JsonResponse(true);
        }

        /**
         * @Route("/application/{id}/{action}", name="application_action", requirements={"id"="\d+", "action"="1|2"})
         */
        public function applicationAction(Request $request, $id, $action){
            if(!$request->isXmlHttpRequest())
                throw new BadRequestHttpException();

            $em = $this->getDoctrine()->getManager();
            $application = $em->getRepository(Application::class)->find($id);
            if(!$application)
                throw new NotFoundHttpException();

            $application->setStatus($action);
            $notification = new Notification(
                $application->getUser(),
                'Pracodawca rozpatrzył Twoją aplikację',
                $this->generateUrl('advertisement', array('id' => $application->getAd()->getId())));
            $em->persist($application);
            $em->persist($notification);
            $em->flush();
            return new JsonResponse(Application::getStatusString($action));
        }

        /**
         * @Route("/application/{id}/task", name="application_task", requirements={"id"="\d+"})
         */
        public function taskAction(Request $request, $id){
            $em = $this->getDoctrine()->getManager();
            $application = $em->getRepository(Application::class)->find($id);
            if(!$application)
                throw new NotFoundHttpException();

            if(!$this->isGranted('task', $application))
                throw new AccessDeniedHttpException();

            $form = $this->createForm(TaskType::class);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $data = $form->getData();
                $data->setApplication($application);
                $application->setStatus(Application::STATUS_TASK);
                $em->persist($data);
                $em->persist($application);
                $em->flush();
                $notification = new Notification($application->getUser(),
                    'Pracodawca przyznał Ci zadanie rekrutacyjne do wykonania',
                    $this->generateUrl('advertisement', array('id' => $application->getAd()->getId())));
                $em->persist($notification);
                $em->flush();
                return $this->redirectToRoute('advertisement_users', array('id' => $application->getAd()->getId()));
            }

            return $this->render('ads/task.html.twig', array(
                'form' => $form->createView(),
                'id' => $id,
            ));
        }

        /**
         * @Route("/task/{id}", name="task_fill", requirements={"id"="\d+"})
         */
        public function taskFillAction(Request $request, $id){
            $em = $this->getDoctrine()->getManager();
            $task = $em->getRepository(Task::class)->getWithJoins($id);
            if(!$task)
                throw new NotFoundHttpException();

            if(!$this->isGranted('fill', $task))
                throw new AccessDeniedHttpException();

            if(in_array($task->getApplication()->getStatus(), [Application::STATUS_ACCEPTED, Application::STATUS_REJECTED]))
                return $this->redirectToRoute('advertisement', array('id' => $task->getApplication()->getAd()->getId()));

            if($task->getAnswer())
                return $this->render('ads/task_fill.html.twig', array(
                    'task' => $task
                ));

            $form = $this->createForm(TaskFillType::class, $task);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $application = $task->getApplication();
                $application->setStatus(Application::STATUS_TASK_ANSWERED);
                $notification = new Notification($application->getAd()->getUser(),
                    $this->getUser()->getFullname() . ' wypełnił zadanie rekrutacyjne',
                    $this->generateUrl('advertisement_users', array('id' => $task->getApplication()->getAd()->getId())).'#'.$task->getApplication()->getUser()->getId());
                $em->persist($task);
                $em->persist($application);
                $em->persist($notification);
                $em->flush();
                return $this->redirectToRoute('advertisement', array('id' => $task->getApplication()->getAd()->getId()));
            }

            return $this->render('ads/task_fill.html.twig', array(
                'form' => $form->createView(),
                'id' => $id,
            ));
        }

        /**
         * @Route("/task/{id}/review", name="task_review", requirements={"id"="\d+"})
         */
        public function taskReviewAction(Request $request, $id){
            $em = $this->getDoctrine()->getManager();
            $task = $em->getRepository(Task::class)->getWithJoins($id);
            if(!$task)
                throw new NotFoundHttpException();
            if(!$this->isGranted('review', $task))
                throw new AccessDeniedHttpException();
            $application = $task->getApplication();
            if($task->getVerified())
                return $this->redirectToRoute('advertisement_users', array('id' => $application->getAd()->getId()));

            if(!$task->getVerified() && $action = $request->query->get('action')){
                if(in_array($action, ['accept', 'reject'])){
                    $task->setVerified(true);
                    switch($action){
                        case 'accept':{
                            $application->setStatus(Application::STATUS_ACCEPTED);
                            break;
                        }
                        case 'reject':{
                            $application->setStatus(Application::STATUS_REJECTED);
                            break;
                        }
                    }
                }
                $notification = new Notification($application->getUser(),
                    'Pracodawca sprawdził Twoje zadanie rekrutacyjne',
                    $this->generateUrl('advertisement', array('id' => $application->getAd()->getId())));
                $em->persist($task);
                $em->persist($application);
                $em->persist($notification);
                $em->flush();
                return $this->redirectToRoute('advertisement_users', array('id' => $application->getAd()->getId(), '_fragment' => $application->getUser()->getId()));
            }

            return $this->render('ads/task_fill.html.twig', array(
                'task' => $task
            ));
        }

        /**
         * @Route("/advertisement/search", name="advertisement_search")
         */
        public function searchAction(Request $request, AdvertisementSerializer $serializer){
            if($value = $request->request->get('value')){
                $ads = $this->getDoctrine()->getRepository(Advertisement::class)->search($value);
                return JsonResponse::fromJsonString($serializer->serialize($ads, array('id', 'job', 'description', 'minSalary', 'maxSalary')));
            }
            throw new BadRequestHttpException();
        }
    }