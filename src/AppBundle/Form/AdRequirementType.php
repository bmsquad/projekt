<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\AdRequirement;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class AdRequirementType extends AbstractType{

        public function buildForm(FormBuilderInterface $builder, array $options){
            $builder
                ->add('value', TextType::class, array(
                    'label' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ));
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => AdRequirement::class,
                'translation_domain' => false
            ));
        }
    }