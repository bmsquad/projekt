<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\Task;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class TaskType extends AbstractType{

        public function buildForm(FormBuilderInterface $builder, array $options){
            $builder
                ->add('task', TextType::class, array(
                    'label' => 'Treść zadania',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Utwórz',
                    'attr' => array(
                        'class' => 'btn btn-main'
                    )
                ));
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => Task::class,
                'translation_domain' => false
            ));
        }
    }