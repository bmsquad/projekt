<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\User;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\Form\Extension\Core\Type\PasswordType;
    use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class UserType extends AbstractType{

        public function buildForm(FormBuilderInterface $builder, array $options){
            $builder
                ->add('name', TextType::class, array(
                    'label' => 'Imię',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('surname', TextType::class, array(
                    'label' => 'Nazwisko',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('mail', EmailType::class, array(
                    'label' => 'Adres email',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('password', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'options' => array(
                        'attr' => array(
                            'class' => 'form-control'
                        )
                    ),
                    'first_options' => array(
                        'label' => 'Hasło'
                    ),
                    'second_options' => array(
                        'label' => 'Powtórz hasło'
                    )
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Zarejestruj się',
                    'attr' => array(
                        'class' => 'btn btn-main'
                    )
                ));

            $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event){
                $role = $event->getForm()->getConfig()->getName();
                $data = $event->getData();
                $data->setRole('ROLE_' . strtoupper($role));
                $data->setPassword(password_hash($data->getPassword(), PASSWORD_DEFAULT));
            });
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => User::class,
                'translation_domain' => false
            ));
        }
    }