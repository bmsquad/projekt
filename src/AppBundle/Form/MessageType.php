<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\Message;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\HiddenType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

    class MessageType extends AbstractType {

        private $tokenStorage;

        public function __construct(TokenStorageInterface $tokenStorage){
            $this->tokenStorage = $tokenStorage;
        }

        public function buildForm(FormBuilderInterface $builder, array $options){

            $builder
                ->add('value',TextType::class, array(
                    'label' => false,
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'Treść'
                    )
                ))
                ->add('submit',SubmitType::class,array(
                    'label' => 'Wyślij',
                    'attr' => array(
                        'class' => 'btn btn-main btn-block'
                    )
                ));

            $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event){
                $user = $this->tokenStorage->getToken()->getUser();
                $data = $event->getData();
                $data->setSender($user);
            });
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => Message::class,
                'translation_domain' => false
            ));
        }
    }