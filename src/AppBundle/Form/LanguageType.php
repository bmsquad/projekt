<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\Language;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

    class LanguageType extends AbstractType {

        private $tokenStorage;

        public function __construct(TokenStorageInterface $tokenStorage){
            $this->tokenStorage = $tokenStorage;
        }

        public function buildForm(FormBuilderInterface $builder, array $options){

            $builder->add('language',TextType::class, array(
                    'label' => 'Jezyk',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('level',ChoiceType::class, array(
                    'label' => 'Poziom',
                    'placeholder' => '',
                    'choices' => array(
                        'A1 - początkujący' => 'A1 - początkujący',
                        'A2 – niższy średnio zaawansowany' => 'A2 – niższy średnio zaawansowany',
                        'B1 – średnio zaawansowany' => 'B1 – średnio zaawansowany',
                        'B2 – wyższy średnio zaawansowany' => 'B2 – wyższy średnio zaawansowany',
                        'C1 – zaawansowany' => 'C1 – zaawansowany',
                        'C2 – profesjonalny' => 'C2 – profesjonalny'
                    ),
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ));


            $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event){
                $user = $this->tokenStorage->getToken()->getUser();
                $data = $event->getData();
                $data->setUser($user);
            });
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => Language::class,
                'translation_domain' => false
            ));
        }
    }