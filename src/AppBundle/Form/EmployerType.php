<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\User;
    use AppBundle\Form\EventListener\PhotoRemoveListener;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\Form\Extension\Core\Type\FileType;
    use Symfony\Component\Form\Extension\Core\Type\HiddenType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TelType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class EmployerType extends AbstractType{

        public function buildForm(FormBuilderInterface $builder, array $options){
            $builder
                ->add('photo', FileType::class, array(
                    'label' => 'Zmień zdjęcie',
                    'required' => false,
                    'attr' => array(
                        'class' => 'd-none',
                        'accept' => 'image/*'//'image/gif, image/png, image/jpeg'
                    ),
                    'label_attr' => array(
                        'class' => 'btn btn-main btn-block'
                    )
                ))
                ->add('photo-remove', HiddenType::class, array(
                    'mapped' => false,
                    'data' => 0
                ))
                ->add('name', TextType::class, array(
                    'label' => 'Imię',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('surname', TextType::class, array(
                    'label' => 'Nazwisko',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('mail', EmailType::class, array(
                    'label' => 'Adres email',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('number', TelType::class, array(
                    'label' => 'Nr telefonu',
                    'attr' => array(
                        'class' => 'form-control',
                        'pattern' => '\+[0-9]{2} [0-9]{3} [0-9]{3} [0-9]{3}',
                        'placeholder' => '+48 123 456 789'
                    ),
                    'required' => false
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Zapisz',
                    'attr' => array(
                        'class' => 'btn btn-main btn-block'
                    )
                ));

            //$builder->addEventSubscriber(new PhotoRemoveListener());
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => User::class,
                'translation_domain' => false
            ));
        }
    }