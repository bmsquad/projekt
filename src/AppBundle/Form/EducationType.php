<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\Education;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

    class EducationType extends AbstractType {

        private $tokenStorage;

        public function __construct(TokenStorageInterface $tokenStorage){
            $this->tokenStorage = $tokenStorage;
        }

        public function buildForm(FormBuilderInterface $builder, array $options){

            $builder->add('datefrom',DateType::class, array(
                    'label' => 'Początek',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'widget' => 'single_text'
                ))
                ->add('dateto',DateType::class, array(
                    'label' => 'Koniec',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'widget' => 'single_text',
                    'required' => false
                ))
                ->add('name',TextType::class, array(
                    'label' => 'Szkoła',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('direction',TextType::class, array(
                    'label' => 'Kierunek',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('level',ChoiceType::class, array(
                    'label' => 'Poziom wykształcenia',
                    'placeholder' => '',
                    'choices' => array(
                        'niższe' => 'niższe',
                        'średnie' => 'średnie',
                        'wyższe' => 'wyższe',
                    ),
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ));


            $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event){
                $user = $this->tokenStorage->getToken()->getUser();
                $data = $event->getData();
                $data->setUser($user);
            });
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => Education::class,
                'translation_domain' => false
            ));
        }
    }