<?php

    namespace AppBundle\Form;

    use AppBundle\Entity\Advertisement;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\CollectionType;
    use Symfony\Component\Form\Extension\Core\Type\FormType;
    use Symfony\Component\Form\Extension\Core\Type\IntegerType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class AdvertisementType extends AbstractType{

        public function buildForm(FormBuilderInterface $builder, array $options){
            $data = $builder->getData();
            $builder
                ->add('job', TextType::class, array(
                    'label' => 'Posada',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add(
                    $builder->create('salary', FormType::class, array(
                        'inherit_data' => true,
                        'label' => 'Płaca'
                    ))
                    ->add('minsalary', IntegerType::class, array(
                        'label' => false,
                        'attr' => array(
                            'class' => 'form-control text-success'
                        )
                    ))
                    ->add('maxsalary', IntegerType::class, array(
                        'label' => false,
                        'attr' => array(
                            'class' => 'form-control text-success'
                        ),
                        'required' => false
                    ))
                    ->add('salarytype', ChoiceType::class, array(
                        'label' => false,
                        'attr' => array(
                            'class' => 'form-control'
                        ),
                        'choices' => array(
                            'netto' => 'netto',
                            'brutto' => 'brutto'
                        ),
                        'placeholder' => ''
                    ))
                )
                ->add('description', TextareaType::class, array(
                    'label' => 'Opis',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('requirements', CollectionType::class, array(
                    'label' => 'Wymagania',
                    'entry_options' => array(
                        'label' => false
                    ),
                    'attr' => array(
                        'class' => 'prototype',
                        'data-index' => $data ? $data->getRequirements()->count() : 0,
                    ),
                    'entry_type' => AdRequirementType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false
                ))
                ->add('tags', TextType::class, array(
                    'label' => 'Tagi',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => $data ? 'Zapisz' : 'Utwórz',
                    'attr' => array(
                        'class' => 'btn btn-main'
                    )
                ));
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => Advertisement::class,
                'translation_domain' => false
            ));
        }
    }