<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\Task;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class TaskFillType extends AbstractType{

        public function buildForm(FormBuilderInterface $builder, array $options){
            $builder
                ->add('answer', TextareaType::class, array(
                    'label' => 'Twoja odpowiedź',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Prześlij',
                    'attr' => array(
                        'class' => 'btn btn-main'
                    )
                ));
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => Task::class,
                'translation_domain' => false
            ));
        }
    }