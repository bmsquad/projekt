<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\Skill;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

    class SkillType extends AbstractType {

        private $tokenStorage;

        public function __construct(TokenStorageInterface $tokenStorage){
            $this->tokenStorage = $tokenStorage;
        }

        public function buildForm(FormBuilderInterface $builder, array $options){

            $builder->add('description',TextareaType::class, array(
                    'label' => ' ',
                    'attr' => array(
                        'class' => 'form-control mt-2',
                        'placeholder' => 'Krótki opis'
                    ),
                    'label_attr' => array(
                        'style' => 'width: 1px;'
                    )
                ));

            $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event){
                $user = $this->tokenStorage->getToken()->getUser();
                $data = $event->getData();
                $data->setUser($user);
            });
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => Skill::class,
                'translation_domain' => false
            ));
        }
    }