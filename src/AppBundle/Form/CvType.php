<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\User;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\CollectionType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\Form\Extension\Core\Type\FileType;
    use Symfony\Component\Form\Extension\Core\Type\FormType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TelType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\HttpFoundation\File\File;
    use Symfony\Component\OptionsResolver\OptionsResolver;

    class CvType extends AbstractType{
        public function buildForm(FormBuilderInterface $builder, array $options){
            $data = $builder->getData();
            $builder
                ->add(
                    $builder->create('data', FormType::class, array(
                        'inherit_data' => true,
                        'label' => 'Dane kontaktowe',
                        'label_attr' => array(
                            'class' => 'profile-header'
                        )
                    ))
                    ->add('photo', FileType::class, array(
                        'label' => 'Zmień zdjęcie',
                        'required' => false,
                        'attr' => array(
                            'class' => 'd-none',
                            'accept' => 'image/*'//'image/gif, image/png, image/jpeg'
                        ),
                        'label_attr' => array(
                            'class' => 'btn btn-main btn-block'
                        )
                    ))
                    ->add('name', TextType::class, array(
                        'label' => 'Imię',
                        'attr' => array(
                            'class' => 'form-control'
                        )
                    ))
                    ->add('surname', TextType::class, array(
                        'label' => 'Nazwisko',
                        'attr' => array(
                            'class' => 'form-control'
                        )
                    ))
                    ->add('mail', EmailType::class, array(
                        'label' => 'Adres email',
                        'attr' => array(
                            'class' => 'form-control'
                        )
                    ))
                    ->add('number', TelType::class, array(
                        'label' => 'Nr telefonu',
                        'attr' => array(
                            'class' => 'form-control',
                            'pattern' => '\+[0-9]{2} [0-9]{3} [0-9]{3} [0-9]{3}',
                            'placeholder' => '+48 123 456 789'
                        ),
                        'required' => false
                    ))
                    ->add('address', TextType::class, array(
                        'label' => 'Adres',
                        'attr' => array(
                            'class' => 'form-control'
                        ),
                        'required' => false
                    ))
                    ->add('birthdate', DateType::class, array(
                        'label' => 'Data urodzenia',
                        'attr' => array(
                            'class' => 'form-control'
                        ),
                        'widget' => 'single_text',
                        'required' => false
                    ))
                )
                ->add(
                    $builder->create('other', FormType::class, array(
                        'inherit_data' => true,
                        'label' => false
                    ))
                        ->add('experience', CollectionType::class, array(
                            'entry_type' => ExperienceType::class,
                            'entry_options' => array(
                                'label' => false
                            ),
                            'allow_add' => true,
                            'allow_delete' => true,
                            'by_reference' => false,
                            'attr' => array(
                                'class' => 'prototype',
                                'data-index' => $data->getExperience()->count()
                            ),
                            'label' => 'Doświadczenie',
                            'label_attr' => array(
                                'class' => 'profile-header'
                            )
                        ))
                        ->add('education', CollectionType::class, array(
                            'entry_type' => EducationType::class,
                            'entry_options' => array(
                                'label' => false
                            ),
                            'allow_add' => true,
                            'allow_delete' => true,
                            'by_reference' => false,
                            'attr' => array(
                                'class' => 'prototype',
                                'data-index' => $data->getEducation()->count()
                            ),
                            'label' => 'Edukacja',
                            'label_attr' => array(
                                'class' => 'profile-header'
                            )
                        ))
                        ->add('language', CollectionType::class, array(
                            'entry_type' => LanguageType::class,
                            'entry_options' => array(
                                'label' => false
                            ),
                            'allow_add' => true,
                            'allow_delete' => true,
                            'by_reference' => false,
                            'attr' => array(
                                'class' => 'prototype',
                                'data-index' => $data->getLanguage()->count()
                            ),
                            'label' => 'Języki',
                            'label_attr' => array(
                                'class' => 'profile-header'
                            )
                        ))
                        ->add('skill', CollectionType::class, array(
                            'entry_type' => SkillType::class,
                            'entry_options' => array(
                                'label' => false
                            ),
                            'allow_add' => true,
                            'allow_delete' => true,
                            'by_reference' => false,
                            'attr' => array(
                                'class' => 'prototype',
                                'data-index' => $data->getSkill()->count()
                            ),
                            'label' => 'Umiejętności',
                            'label_attr' => array(
                                'class' => 'profile-header'
                            )
                        ))
                )
                ->add('submit', SubmitType::class, array(
                    'label' => 'Zapisz',
                    'attr' => array(
                        'class' => 'btn btn-main btn-block mt-2'
                    )
                ));
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => User::class,
                'translation_domain' => false
            ));
        }
    }