<?php
    namespace AppBundle\Form;

    use AppBundle\Entity\Experience;
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\FormEvent;
    use Symfony\Component\Form\FormEvents;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

    class ExperienceType extends AbstractType {

        private $tokenStorage;

        public function __construct(TokenStorageInterface $tokenStorage){
            $this->tokenStorage = $tokenStorage;
        }

        public function buildForm(FormBuilderInterface $builder, array $options){

            $builder->add('datefrom',DateType::class, array(
                'label' => 'Początek',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'widget' => 'single_text'
            ))
            ->add('dateto',DateType::class, array(
                'label' => 'Koniec',
                'attr' => array(
                    'class' => 'form-control'
                ),
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('value',TextType::class, array(
                'label' => 'Stanowisko',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('description', TextareaType::class, array(
                'label' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Krótki opis'
                )
            ));

            $builder->addEventListener(FormEvents::SUBMIT, function(FormEvent $event){
                $user = $this->tokenStorage->getToken()->getUser();
                $data = $event->getData();
                $data->setUser($user);
            });
        }

        public function configureOptions(OptionsResolver $resolver){
            $resolver->setDefaults(array(
                'data_class' => Experience::class,
                'translation_domain' => false
            ));
        }
    }