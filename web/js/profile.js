$('.add').click(function(){
    let $protoEl = $(this).parent().siblings('.prototype');
    let proto = $protoEl.attr('data-prototype');
    let index = $protoEl.attr('data-index');
    proto = proto.replace(/__name__/g, index);
    $protoEl.attr('data-index', Number(index) + 1);
    $protoEl.append(proto);
});
$('.edit').click(function(){
    let $editable = $(this).siblings('.editable');
    let $userData = $(this).siblings('.user-data');
    if($editable.children('div').html() === '')
        $editable.find('.add').click();
    $(this).hide();
    if($userData.hasClass('has-bullets'))
        $userData.find('.bullet').attr('disabled', true).first().on('transitionend', function(){
            $userData.slideUp(() => {
                $editable.slideDown();
            });
        });
    else
        $userData.slideUp(() => {
            $editable.slideDown();
        });
});
$(document).on('click', '.remove', function(){
    $(this).closest('.form-group').slideUp(function(){
        $(this).remove();
    });
});
$('input[type=file]').change(function(){
    if(this.files && this.files[0]){
        let reader = new FileReader();
        reader.onload = function(e){
            $('.image-container img').attr('src', e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
    }
});
$('input[type=tel]').blur(function(){
    let value = $(this).val();
    if(value.length >= 12){
        let regex = /\+[0-9]{2} [0-9]{3} [0-9]{3} [0-9]{3}/;
        if(!regex.test(value)){
            [...value] = value.replace(/ /g, '');
            value.splice(3, 0, ' '); value.splice(7, 0, ' '); value.splice(11, 0, ' ');
            value = value.reduce((a, b) => a + b);
            $(this).val(value);
        }
    }
});