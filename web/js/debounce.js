function debounce(func, timeout){
    if(typeof(func) !== 'function')
        return;
    clearTimeout(window.timer);
    window.timer = setTimeout(function(){
        func();
    }, timeout);
}