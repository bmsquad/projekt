const userData = data => `
    <div class='py-2'>
        <span class='font-weight-bold' style='font-size: 20px; color: var(--main);'>Doświadczenie</span>
        ${ !data.experience.length ? `
            <div class='user-data'>
                <div class='pl-3 py-1'>Brak danych</div>
            </div>
        `: `
            <div class='user-data has-bullets ml-2' style='border-left: 3px solid #3b5998;'>
                <div class='pl-3 py-1'>
                    ${ JSON.parse(data.experience).map( experience => `
                        <div class='bullet'>
                            <div class='row mb-2'>
                                <div class='col-4'>${ experience.dateFrom + (experience.dateTo ? ' - ' + experience.dateTo : '')}</div>
                                <div class='col-8'>
                                    <div style='font-size: 18px; font-weight: bold;'>${ experience.value }</div>
                                    <div class='text-muted'>${ experience.description }</div>
                                </div>
                            </div>
                        </div>
                    `)}
                </div>
            </div>
        `
        }
    </div>  
`;